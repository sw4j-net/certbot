# Introduction

This project creates a docker image with [certbot](https://certbot.eff.org/) installed.

The primary use of this image is to enable requesting/renewing [Let's Encrypt](https://letsencrypt.org/) certificates
for gitlab pages.

This repository is mirrored to https://gitlab.com/sw4j-net/certbot

## Usage

Create a job in `.gitlab-ci.yml` with the following content:
```
renew_cert:
  stage: deploy
  only:
    variables:
      - $RENEW == "true"
  script:
    - echo "Renew Cert"
    - git config --global user.name ${GITLAB_USER_LOGIN}
    - git config --global user.email ${GITLAB_USER_EMAIL}
    - renew.sh
```

Used environment variables:
* HOST_NAME (required) the host name for the certificate
* RENEW required for the job to run
* RENEW_TOKEN an access token for the repository
