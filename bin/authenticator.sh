#!/bin/sh

mkdir -p well-known/acme-challenge
echo ${CERTBOT_VALIDATION} > well-known/acme-challenge/${CERTBOT_TOKEN}
git add well-known/acme-challenge/${CERTBOT_TOKEN}
git commit -m "GitLab runner - Added certbot challenge file for certificate renewal"
git push https://${GITLAB_USER_LOGIN}:${RENEW_TOKEN}@gitlab.com/$CI_PROJECT_PATH HEAD:$CI_COMMIT_REF_NAME

interval_sec=15
max_tries=80 # 20 minutes
n_tries=0
while [ $n_tries -le $max_tries ]
do
  status_code=$(curl -L --write-out "%{http_code}\n" --silent --output /dev/null http://${HOST_NAME}/.well-known/acme-challenge/${CERTBOT_TOKEN})
  echo "Debug: HTTP-CODE $status_code"
  if [ $status_code -eq 200 ]; then
    exit 0
  fi

  n_tries=$((n_tries+1))
  sleep $interval_sec
done

exit 1
