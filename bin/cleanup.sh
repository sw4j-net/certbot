#!/bin/sh

echo "Removing challenge file"
git rm -r well-known
git commit -m "GitLab runner - Removed certbot challenge file"
git push https://${GITLAB_USER_LOGIN}:${RENEW_TOKEN}@gitlab.com/$CI_PROJECT_PATH HEAD:$CI_COMMIT_REF_NAME
