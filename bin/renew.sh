#!/bin/sh

EXEC_DIR=$(dirname $(readlink -f $0))

certbot certonly \
  --manual \
  --preferred-challenges=http \
  -m $GITLAB_USER_EMAIL \
  --agree-tos \
  --manual-auth-hook authenticator.sh \
  --manual-cleanup-hook cleanup.sh \
  --manual-public-ip-logging-ok \
  -d ${HOST_NAME}

curl --request PUT --header "PRIVATE-TOKEN: ${RENEW_TOKEN}" \
  --form "certificate=@/etc/letsencrypt/live/${HOST_NAME}/fullchain.pem" \
  --form "key=@/etc/letsencrypt/live/${HOST_NAME}/privkey.pem" \
  https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pages/domains/${HOST_NAME}
