ARG CI_REGISTRY
FROM debian:bookworm-backports

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get install -y git curl certbot
EOF

COPY bin/*.sh /bin/
